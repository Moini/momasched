[
  # Post 1
  {
    'scheduled_date': "27.08.2023 05:32",
    'media': "/tmp/testimage.png",
    'media_description': "This is just a test image. Don't worry about it.",
    'text': """Hi, this is a test text! 

If you can see this, my new scheduling script works even when I'm sleeping. 

äüß&"';<

💖

#test #mastodon_api
"""
  },
  # Post 2
  {
    'scheduled_date': "01.09.2023 10:00",
    'media': "/tmp/testimage.png",
    'media_description': "This is just another test image. Don't worry about it."
    'text': """Hi, this is another test text! 

If you can see this, my new scheduling script really works 

💖

#test #mastodon_api
"""
  }
,
]