#!/bin/bash
if [ -f "local.conf" ]; then
  echo "Keeping old config file."
else
  cp config.template momasched.conf
fi
mkdir pythonenv
python3 -m venv pythonenv
source pythonenv/bin/activate 
pip install --upgrade pip
pip install -r requirements.txt

echo 'If you haven't yet, fill in the data into your momasched.conf file now.'
echo 'Then, if this is the first time you use this app with this specific Mastodon instance (or you used it with a different server before), run:'
echo 'python3 src/momasched.py --firstrun'

echo 'For more usage hints, type:'
echo 'python3 src/momasched.py --help'