#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2023 Maren Hachmann
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

"""
Small program for scheduling Mastodon posts - if called with a file, it will schedule the posts in the file (needs to be formatted as arrays). If called without parameters, it will print a list of scheduled posts for the account.
"""

from mastodon import Mastodon
import configparser
import os
import argparse
import hashlib
from datetime import datetime
import ast
import html

class MoMaSched():
    def __init__(self):

        # Evaluate command line arguments
        parser = argparse.ArgumentParser("momasched.py")
        parser.add_argument("-i", "--input", help="Path to input file which contains a dictionary of posts to schedule", type=str, dest="post_file", default="")
        parser.add_argument("-q", "--query", help="Print status of scheduled posts, True or False", type=bool, default=False, dest='is_query')
        parser.add_argument("--firstrun", help="Use this to register your app with your Mastodon instance. Creates a secret key file, do not delete. True or False.", type=bool, default=False, dest='is_firstrun')
        parser.add_argument("--delete_id", help="Delete post with the given ID", type=str, default="", dest="delete_id")
        self.args = parser.parse_args()

        # Evaluate config file
        config_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'momasched.conf')

        config = configparser.ConfigParser()
        config.read(config_file)
        self.PASSWORD = config.get('CREDENTIALS', 'MastodonPassword', raw=True)
        self.MASTO_SERVER = config['CREDENTIALS']['MastodonServer']
        self.ACCOUNT = config['CREDENTIALS']['AccountEmail']
        self.DEBUG = config['GENERAL']['Debug']

    def run(self):
        if self.args.is_query:
            self.get_scheduled_status()
        elif self.args.is_firstrun:
            self.first_run()
        elif self.args.delete_id != "":
            self.delete_post()
        elif os.path.isfile(self.args.post_file):
            self.toot_posts()
        else:
            print("Please either indicate a valid posts file, or query the post status. More info with -h.")

    def login(self):
        mastodon = Mastodon(
            client_id='momasched_clientcred.secret', 
            api_base_url = self.MASTO_SERVER
        )
        mastodon.log_in(
            self.ACCOUNT,
            self.PASSWORD,
            to_file = 'momasched_usercred.secret'
        )
        return(mastodon)

    def first_run(self):
        print(self.MASTO_SERVER)
        Mastodon.create_app(
            'momasched',
            website='https://gitlab.com/Moini/momasched',
            api_base_url = self.MASTO_SERVER,
            to_file = 'momasched_clientcred.secret',
            # scopes = ['read:statuses', 'write:media', 'write:statuses']
        )


    def get_scheduled_status(self):
        mastodon = self.login()

        server_response = mastodon.scheduled_statuses()

        for toot in server_response:
            print(toot['params']['text'])
            print(toot['scheduled_at'])
            print(toot['media_attachments'])
            print('\n')

        # logout
        mastodon.revoke_access_token()

    def delete_post(self):
        # unfortunately does not delete scheduled posts, but only published ones

        mastodon = self.login()

        server_response = mastodon.status_delete(self.args.delete_id)

        print(server_response)

        # logout
        mastodon.revoke_access_token()

    def toot_posts(self):
        posts = self.get_posts()

        mastodon = self.login()

        for post in posts:

            # first upload picture
            if 'media' in post and post['media'] != "":
                if os.path.isfile(post['media']):
                    media_response = mastodon.media_post(post['media'], description=post['media_description'], focus=(0,0))
                
                    media_id = media_response['id']
                else:
                    raise Exception('Image file {} not found'.format(post['media']))
            else:
                media_id = None

            # hash post content, so we can use it to prevent posting the identical thing twice
            status_hash = hashlib.md5(post['text'].encode('utf-8')).hexdigest()

            # schedule the post
            if self.DEBUG == '1':
                # only private posts
                server_response = mastodon.status_post(post['text'], 
                    media_ids=[media_id],
                    visibility='direct', 
                    idempotency_key=status_hash, 
                    scheduled_at=post['scheduled_date']
                )
            elif self.DEBUG == '0':
                server_response = mastodon.status_post(post['text'],
                    media_ids=[media_id],
                    visibility='public',
                    idempotency_key=status_hash,
                    scheduled_at=post['scheduled_date']
                )

            print(server_response)
        
        # logout
        mastodon.revoke_access_token()

    def get_posts(self):

        with open(self.args.post_file, encoding='utf-8-sig') as f:

            posts = ast.literal_eval(f.read().strip())

            for post in posts:
                # no HTML escaping necessary, linebreaks accepted as is

                # Convert string to datetime object
                post['scheduled_date'] = datetime.strptime(post['scheduled_date'], "%d.%m.%Y %H:%M")

                # make sure any image has a description:
                if 'media' in post and post['media'] != "" and ('media_description' not in post or 
                    post['media_description'].strip()) == "":
                    raise Exception('Please add a description for image {}.'.format(post['media']))
        return(posts)


if __name__ == '__main__':
    MoMaSched().run()

